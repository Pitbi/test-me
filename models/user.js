var mongoose = require('mongoose')

var UserSchema = mongoose.Schema({
    date: {type: Date, default: Date.now},
    name: String
})

module.exports = mongoose.model('User', UserSchema)

