var express = require('express')
var jade = require('jade')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var i18n = require('i18n')

var User = require('./models/user')

const PORT = process.env.PORT || 5000

//mongoose.connect('mongodb://localhost/testme')

var app = express()

i18n.configure({
    locales:['fr', 'nl', 'en'],
    defaultLocale: 'fr',
    directory: __dirname + '/node_modules/betterstreetadmin-i18n/'
})
i18n.init()

app.use(bodyParser.urlencoded())
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')

app.use((req, res, next) => {
    if (res.locals.__)
        res.locals.__ = i18n.__
    next()
})

app.post('/subscribe', (req, res) => {
    User.create(req.body, (err) => {
        if (err) res.status(500).send('Bad!')

        res.redirect('/')
    })
})

app.get('/', (req, res) => {
    //User.find((err, users) => {
       res.render('index', { users: {} })
    //})
})

var server = app.listen(PORT, () => {
	console.info(`Server listening port ${PORT}`)	
})

